<?php

declare(strict_types=1);

namespace App\Presenters;


final class HomepagePresenter extends BasePresenter
{
	public function actionDefault(): void
	{
		$this->template->response = null;
	}

	public function handleDownload() {
		if (!$this->isAjax()) {
			return;
		}

		$client = new \GuzzleHttp\Client();
		$response = $client->request('GET', 'https://dog.ceo/api/breeds/image/random');
		$body = (string) $response->getBody();
		
		$json = json_decode($body, true);
		$this->template->response = $json["message"];

		$this->redrawControl("ajaxSnippet");
	}
}
